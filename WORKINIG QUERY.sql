WITH TABLE_A_temp (DIMENSION_1, DIMENSION_2, MEASURE_1, MEASURE_2) AS 
(
    select DIMENSION_1, DIMENSION_2, MEASURE_1, 0 as MEASURE_2 from TABLE_A
    union 
    select DIMENSION_1, DIMENSION_2, 0 as MEASURE_1, MEASURE_2 from TABLE_B
)

select TABLE_A_temp.DIMENSION_1 as dimension_1, CORRECT_DIMENSION_2 as dimension_2, ZEROIFNULL(sum(MEASURE_1)) as measure_1 , ZEROIFNULL (sum(MEASURE_2)) as measure_2 
from  TABLE_A_temp
join (select distinct * from table_map) using(DIMENSION_1)
group by 1,2
order by 1